import { ArticleProvider } from './../../providers/article/article';
import { EditPage } from './../edit/edit';
import { PopoverPage } from './../popover/popover';
// import { ProfilePage } from "./profile";
import { Component, OnInit } from '@angular/core';
// import{ArticleProvider}
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ViewController
} from 'ionic-angular';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit {
  articles: any[];
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private popoverCtrl: PopoverController,
    private artpro: ArticleProvider
  ) {}

  showpopdown(newevent) {
    let popover = this.popoverCtrl.create(PopoverPage);

    popover.present({
      ev: newevent
    });
  }

  editArt() {
    this.navCtrl.push('EditPage');
  }

  ngOnInit() {
    this.getArticle();
  }

  getArticle(): void {
    this.artpro.getArticle().subscribe(data => {
      this.articles = data;
      console.log(data);
    });
  }
}
