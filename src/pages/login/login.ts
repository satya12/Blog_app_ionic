import { SignupPage } from './../signup/signup';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './../../app/models/user';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from '@angular/forms';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController
} from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
// import { FirebaseAuth } from "@firebase/auth-types";
import firebase from 'firebase';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  user = {} as User;
  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;

  google = {
    loggedin: false
  };
  constructor(
    private toast: ToastController,
    private formbuilder: FormBuilder,
    private ngAuth: AngularFireAuth,
    private navCtrl: NavController,
    private navParams: NavParams,
    private load: LoadingController
  ) {
    // let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.formgroup = formbuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required, Validators.maxLength[6]]
    });

    this.email = this.formgroup.controls['email'];
    this.password = this.formgroup.controls['password'];
  }

  onsignup() {
    this.navCtrl.setRoot(SignupPage);
  }

  async signin(user: User) {
    var loader = this.load.create({
      content: 'Wait a moment ...',
      duration: 1000
    });
    loader.present();

    try {
      this.navParams.get('data');
      if (this.user == null) console.log('enter details');
      const result = await this.ngAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.password
      );
      console.log(result);
      if (result) {
        this.toast
          .create({
            message: 'successfully logged in',
            duration: 5000,
            showCloseButton: true,
            position: 'bottom'
            // closeButtonText: "Undo"
          })
          .present();
        this.navCtrl.setRoot(ProfilePage);
      } else {
        // this.toast.create({
        //   message: "email not maatched to db",
        //   duration: 3000,
        //   showCloseButton: true
        // });
        console.log(Error);
      }
    } catch (e) {
      console.error(e);
    }

    // profile() {
    //   this.navCtrl.push("ProfilePage");
    // }
  }

  /************************/
  //Login with google //
  /************************/

  loginWithGoogle() {
    this.ngAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        console.log(res);
      });
  }

  logout() {
    this.ngAuth.auth.signOut();
    this.google.loggedin = false;
  }
}
