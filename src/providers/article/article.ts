import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
/*
  Generated class for the ArticleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ArticleProvider {
  private url: string = 'http://localhost:9000/articles';
  constructor(public http: Http) {
    // console.log('Hello ArticleProvider Provider');
  }

  getArticle() {
    return this.http.get(this.url).map((res: Response) => {
      console.log('res:', res);
      return res.json();
    });
  }
}
