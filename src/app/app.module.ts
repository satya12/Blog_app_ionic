import { HttpClient } from "@angular/common/http";
import { config } from "./app.firebase.config";
import { SignupPageModule } from "./../pages/signup/signup.module";
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule, AngularFireAuth } from "angularfire2/auth";
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import {
  IonicApp,
  IonicErrorHandler,
  IonicModule,
  PopoverController,
  LoadingController
} from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
// import { RouterModule } from "@angular/router";
import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { LoginPageModule } from "../pages/login/login.module";
import { ProfilePageModule } from "../pages/profile/profile.module";
import { PopoverPageModule } from "../pages/popover/popover.module";
import { EditPage } from "../pages/edit/edit";
import { EditPageModule } from "../pages/edit/edit.module";
import { ArticleProvider } from "../providers/article/article";
import { HttpModule } from "@angular/http";
@NgModule({
  declarations: [MyApp, HomePage],
  imports: [
    BrowserModule,
    SignupPageModule,
    EditPageModule,
    LoginPageModule,
    ProfilePageModule,
    PopoverPageModule,
    AngularFireAuthModule,
    HttpModule,
    AngularFireModule.initializeApp(config),

    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage],
  providers: [
    ,
    StatusBar,
    SplashScreen,
    PopoverController,
    LoadingController,
    AngularFireAuth,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ArticleProvider
  ]
})
export class AppModule {}
